#pragma once
#include <iostream>
#include "EV.h"

using namespace std;

// funkcja sluzaca do zamieniania dwoch elementow miejscami
void swap(Edge **x, Edge **y)
{
	Edge* temp = *x;
	*x = *y;
	*y = temp;
}

class Heap
{
public:
	Edge* *tablica; // tablica uzywana jako kopiec
	int pojemnosc; // rozmiar tablicy
	int rozmiar; // ilosc elementow w kopcu
public:
	Heap(int edges); // konstruktor
	void MinHeapify(int);
	int parent(int i) { return (i - 1) / 2; }
	void removeMin(); // usuwa pierwszy element z kopca
	Edge* front() { return tablica[0]; } // zwraca wartosc pierwszego elementu
	void insert(Edge *edge); // dodaje nowa wartosc do kopca
};

Heap::Heap(int edges)
{
	rozmiar = 0;
	pojemnosc = edges;
	tablica = new Edge*[pojemnosc];
}

void Heap::insert(Edge *edge)
{
	if (rozmiar == pojemnosc)
	{
		cout << "Kopiec jest pelny!";
		return;
	}

	rozmiar++;
	int i = rozmiar - 1;
	tablica[i] = edge; // umieszczenie nowego elementu na koncu kopca

					   // przywrocenie wlasciwosci kopca
	while (i != 0 && tablica[parent(i)]->weight > tablica[i]->weight)
	{
		swap(&tablica[i], &tablica[parent(i)]);
		i = parent(i);
	}
}

void Heap::removeMin()
{
	//cout << "Rozmiar: " << rozmiar << endl;
	if (rozmiar <= 0) // gdy kopiec jest pusty
		cout << "Kopiec jest pusty!\n";
	if (rozmiar == 1) // gdy w kopcu jest tylko jeden element to zmniejszany jest jego rozmiar
	{
		rozmiar--;
	}

	tablica[0] = tablica[rozmiar - 1]; // ostatni element idzie na pierwsze miejsce
	rozmiar--; // rozmiar zmniejszany o jeden
	MinHeapify(0); // przywrocenie wlasciwosci kopca
}

void Heap::MinHeapify(int i)
{
	int l = 2 * i + 1; // indeks lewego syna
	int r = 2 * i + 2; // indeks prawego syna
	int smallest = i; // najmniejsza wartosc sposrod ocja, lewego syna i prawego syna
	if (l < rozmiar && tablica[l]->weight < tablica[i]->weight) // sprawdzenie czy lewy syn ma mniejsza wartosc niz ojciec
		smallest = l; // lewy syn ustawiany jako smallest
	if (r < rozmiar && tablica[r]->weight < tablica[smallest]->weight) // sprawdzenie czy prawy syn ma mniejsza wartosc niz smallest
		smallest = r; // prawy syn ustawiany jako smallest
	if (smallest != i) // jesli ojciec nie ma najmniejszej wartosci
	{
		swap(&tablica[i], &tablica[smallest]); // zamiana miejscem ojca z synem
		MinHeapify(smallest); // wywolanie rekurencyjne
	}
}