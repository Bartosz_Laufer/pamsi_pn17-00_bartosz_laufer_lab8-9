#pragma once
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Vertex;

class Edge {
public:
	Edge(int waga, Vertex *one, Vertex *two) // konstruktor
	{
		weight = waga; // nadanie wagi
		first = one; // nadanie wierzcholkow ktore laczy dana krawedz
		last = two;
	}
	int weight; // waga krawedzi
	Vertex *first; // jeden koniec krawedzi
	Vertex *last; // drugi koniec krawedzi
	Edge* posF; // wskaznik na pozycje krawedzi na liscie jednego wierzcholka
	Edge* posL; // wskaznik na pozycje krawedzi na liscie drugiego wierzcholka
	Edge* pos; // wskaznik na pozycje krawedzi na liscie
	string etykieta; // uzywane przy sprawdzaniu czy graf jest spojny
};

class Vertex {
public:
	Vertex(int klucz) // konstruktor
	{
		key = klucz;
	}
	int key; // wartosc wierzcholka
	vector<Edge*> adjList2; // lista sasiedztwa danego wierzcholka
	Vertex* pos; // wskaznik na pozycje wierzcholka na liscie
	string etykieta; // uzywane przy sprawdzaniu czy graf jest spojny
	int distance; // odleglosc od wierzcholka poczatkowego (do Dijkstry)
	int indeks; // indeks w kolejce priorytetowej (do Dijkstry)
	int odleglosc; // aktualna waga krawedzi (do Dijkstry)
	Vertex *last; // wierzcholek od ktorego prowadzi krawedz do tego wierzcholka (do Dijkstry)
};