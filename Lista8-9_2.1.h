#pragma once
#include <iostream>
#include "ev.h";

using namespace std;

struct wezel {
	Vertex* ojciec; // wskaznik na ojca
	int ranga; // wielkosc drzewa
};

class klaster {
public:
	wezel *W; // tablica wezlow
	klaster(int n) { // konstruktor
		W = new wezel[n];
	}
	~klaster() { // destruktor
		delete[] W;
	}
	void makeK(Vertex *vertex); // utworzenie klastra
	Vertex* findK(Vertex *vertex);
	void unionK(Edge *edge);
	void display(int vertices);
};

void klaster::makeK(Vertex *vertex)
{
	W[vertex->key].ojciec = vertex;
	W[vertex->key].ranga = 0; // ustawienie wielkosci drzewa
}

Vertex* klaster::findK(Vertex *vertex)
{
	if (W[vertex->key].ojciec->key != vertex->key) // jesli wartosc wierzcholka nie jest rowna wartosci ojca 
		W[vertex->key].ojciec = findK(W[vertex->key].ojciec); // to funkcja wywolywana jest rekurencyjnie dla ojca
	return W[vertex->key].ojciec; // zwrocenie ojca klastra
}

void klaster::unionK(Edge *edge)
{
	Vertex *V1 = findK(edge->first); // ojciec jednego klastra
	Vertex *V2 = findK(edge->last); // ojciec drugiego klastra
	if (V1 != V2) { // sprawdzenie czy korzenie sa rozne
		if (W[V1->key].ranga > W[V2->key].ranga)
			W[V2->key].ojciec = V1; // gdy ranga V1 wieksza to dolaczamy V2
		else {
			W[V1->key].ojciec = V2;
			if (W[V1->key].ranga == W[V2->key].ranga) // gdy ranga V2 wieksza to dolaczamy V1
				W[V2->key].ranga++;
		}
	}
}

// wyswietlenie zawartosci klastra
void klaster::display(int vertices)
{
	for (int i = 0; i < vertices; i++)
		cout << W[i].ojciec->key << " ";
	cout << endl;
	for (int i = 0; i < vertices; i++)
		cout << W[i].ranga << " ";
	cout << endl;
}