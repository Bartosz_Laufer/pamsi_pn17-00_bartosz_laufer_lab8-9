#pragma once
#include <iostream>
#include "EV.h"

using namespace std;

// funkcja sluzaca do zamieniania dwoch elementow miejscami
void swap(Vertex **x, Vertex **y)
{
	Vertex* temp = *x;
	*x = *y;
	*y = temp;
}

class Heap2
{
public:
	Vertex* *tablica; // tablica uzywana jako kopiec
	int pojemnosc; // rozmiar tablicy
	int rozmiar; // ilosc elementow w kopcu
public:
	Heap2(int vertices); // konstruktor
	void MinHeapify(int);
	int parent(int i) { return (i - 1) / 2; }
	void removeMin(); // usuwa pierwszy element z kopca
	Vertex* front() { return tablica[0]; } // zwraca wartosc pierwszego elementu
	void insert(Vertex *vertex); // dodaje nowa wartosc do kopca
	void changeKey(Vertex *vertex, int key); // zamienia wartosc klucza na nowa
	void display();
};

Heap2::Heap2(int vertices)
{
	rozmiar = 0;
	pojemnosc = vertices;
	tablica = new Vertex*[pojemnosc];
}

void Heap2::insert(Vertex *vertex)
{
	if (rozmiar == pojemnosc)
	{
		cout << "Kopiec jest pelny!";
		return;
	}

	rozmiar++;
	int i = rozmiar - 1;
	tablica[i] = vertex; // umieszczenie nowego elementu na koncu kopca
	vertex->index = i;

	// przywrocenie wlasciwosci kopca
	while (i != 0 && tablica[parent(i)]->distance > tablica[i]->distance)
	{
		swap(&tablica[i], &tablica[parent(i)]);
		i = parent(i);
	}
}

void Heap2::removeMin()
{
	//cout << "Rozmiar: " << rozmiar << endl;
	if (rozmiar <= 0) // gdy kopiec jest pusty
		cout << "Kopiec jest pusty!\n";
	if (rozmiar == 1) // gdy w kopcu jest tylko jeden element to zmniejszany jest jego rozmiar
	{
		rozmiar--;
	}
	else {
		tablica[0] = tablica[rozmiar - 1]; // ostatni element idzie na pierwsze miejsce
		tablica[rozmiar - 1]->index = 0;
		rozmiar--; // rozmiar zmniejszany o jeden
		MinHeapify(0); // przywrocenie wlasciwosci kopca
	}
}

void Heap2::MinHeapify(int i)
{
	int l = 2 * i + 1; // indeks lewego syna
	int r = 2 * i + 2; // indeks prawego syna
	int smallest = i; // najmniejsza wartosc sposrod ocja, lewego syna i prawego syna
	if (l < rozmiar && tablica[l]->distance < tablica[i]->distance) // sprawdzenie czy lewy syn ma mniejsza wartosc niz ojciec
		smallest = l; // lewy syn ustawiany jako smallest
	if (r < rozmiar && tablica[r]->distance < tablica[smallest]->distance) // sprawdzenie czy prawy syn ma mniejsza wartosc niz smallest
		smallest = r; // prawy syn ustawiany jako smallest
	if (smallest != i) // jesli ojciec nie ma najmniejszej wartosci
	{
		swap(&tablica[i], &tablica[smallest]); // zamiana miejscem ojca z synem
		int temp = tablica[i]->index;
		tablica[i]->index = tablica[smallest]->index;
		tablica[smallest]->index = temp;
		MinHeapify(smallest); // wywolanie rekurencyjne
	}
}

void Heap2::changeKey(Vertex *vertex, int key)
{
	vertex->distance = key;
	int i = vertex->index;
	// przywrocenie wlasciwosci kopca
	while (i != 0 && tablica[parent(i)]->distance > tablica[i]->distance)
	{
		swap(&tablica[i], &tablica[parent(i)]);
		int temp = tablica[i]->index;
		tablica[i]->index = tablica[parent(i)]->index;
		tablica[parent(i)]->index = temp;
		i = parent(i);
	}
}

void Heap2::display()
{
	cout << rozmiar << ": ";
	for (int i = 0; i < rozmiar; i++)
		cout << tablica[i]->key << " ";
	cout << endl;
}