#pragma once
#include <iostream>
#include "ev.h"

using namespace std;

class Graph {
public:
	Graph(int n); // konstruktor
	vector<Vertex*> vertexList; // tablica wskaznikow na wierzcholki
	vector<Edge*> edgeList; // tablica wskaznikow na krawedzie
	void addEdge(int waga, int key1, int key2); // dodawanie krawedzi
	void addEdge2(int waga, int indeks1, int indeks2); // dodawanie krawedzi
	bool addVertex(int klucz); // dodawanie wierzcholka
	int findVertex(int klucz); // szukanie wierzcholka
	void vertices(); // wyswietlenie wszystkich wierzcholkow w grafie
	void edges(); // wyswietlenie wszystkich krawedzi w grafie
	void incidentEdges(int indeksV); // wyswietlenie wszystkich krawedzi incydentnych danego wierzcholka
	void endVertices(int indeksE); // wyswietlenie wierzcholkow ktore laczy dana krawedz
	void opposite(int indeksV, int indeksE); // zwrocenie wierzcholka przeciwleglego wzgledem danej krawedzi
	Vertex* opposite(Vertex* V, Edge* E); // zwrocenie wierzcholka przeciwleglego wzgledem danej krawedzi
	void replaceV(int indeksV, int nowyKlucz); // zastapienie elementu na wierzcholku innym
	void removeE(int indeksE); // usuwanie krawedzi 
	void removeV(int indeksV); // usuwanie wierzcholka i jego krawedzi incydentnych
};

Graph::Graph(int n)
{
	vertexList.resize(n); // ustawienie rozmiaru wektora odpowiadajacego liczbie wierzcholkow
}

bool Graph::addVertex(int klucz)
{
	Vertex *V1 = new Vertex(klucz); // tworzenie nowego wierzcholka
	vertexList[klucz] = V1; // dodanie wierzcholka do listy
	return true;
}

int Graph::findVertex(int klucz)
{
	for (int i = 0; i < vertexList.size(); i++) {
		if (vertexList[i]->key == klucz)
			return i; // zwraca indeks wierzcholka na liscie wierzcholkow
	}
	return -1; // gdy takiego wierzcholka nie ma w grafie
}

void Graph::addEdge2(int waga, int indeks1, int indeks2)
{
	if (indeks1 >= 0 && indeks2 >= 0 && indeks1 < vertexList.size() && indeks2 < vertexList.size()) { // sprawdzenie czy podane zostaly poprawne indeksy
		Edge *E1 = new Edge(waga, vertexList[indeks1], vertexList[indeks2]); // utworzenie krawedzi
		edgeList.push_back(E1); // dodanie krawedzi do listy
		vertexList[indeks1]->adjList2.push_back(E1); // dodanie krawedzi do list krawedzi dla obu wierzcholkow
		vertexList[indeks2]->adjList2.push_back(E1);
		//cout << "Do wierzcholkow " << vertexList[indeks1]->key << " i " << vertexList[indeks2]->key << " dodano krawedz o wartosci " << waga << endl;
	}
}

void Graph::addEdge(int waga, int key1, int key2)
{
	if (findVertex(key1) == -1 || findVertex(key2) == -1) // sprawdzenie czy podane wierzcholki istnieja
		cout << "Nie mozna dodac krawedzi (co najmniej jeden z podanych wierzcholkow nie istnieje)!\n";
	else {
		Edge *E1 = new Edge(waga, vertexList[findVertex(key1)], vertexList[findVertex(key2)]); // utworzenie krawedzi
		edgeList.push_back(E1); // dodanie krawedzi do listy
		E1->pos = edgeList[edgeList.size() - 1]; // wskaznik na pozycje krawedzi na liscie
		vertexList[findVertex(key1)]->adjList2.push_back(E1); // dodanie krawedzi do list krawedzi dla obu wierzcholkow
		E1->posF = vertexList[findVertex(key1)]->adjList2[vertexList[findVertex(key1)]->adjList2.size() - 1]; // wskaznik na pozycje krawedzi na liscie jednego wierzcholka
		vertexList[findVertex(key2)]->adjList2.push_back(E1);
		E1->posF = vertexList[findVertex(key1)]->adjList2[vertexList[findVertex(key1)]->adjList2.size() - 1]; // wskaznik na pozycje krawedzi na liscie drugiego wierzcholka
		cout << "Do wierzcholkow " << key1 << " i " << key2 << " dodano krawedz o wartosci " << waga << endl;
	}
}

void Graph::vertices()
{
	if (vertexList.size() != 0) { // sprawdzenie czy graf ma jakiekolwiek wierzcholki
		cout << endl << "Wszystkie wierzcholki w grafie:\n";
		for (int i = 0; i < vertexList.size(); i++) {
			cout << vertexList[i]->key << " "; // wyswietlanie kolejnych wierzcholkow
		}
		cout << endl;
	}
	else
		cout << "Nie ma zadnych wierzcholkow w grafie!\n";
}

void Graph::edges()
{
	/*
	if (vertexList.size() == 0) { // sprawdzenie czy graf nie jest pusty
	cout << "Graf jest pusty!\n";
	}
	else {
	cout << endl << "Wszystkie krawedzie w grafie:\n";
	for (int i = 0; i < vertexList.size(); i++) { // petla przechodzaca po wierzcholkach
	cout << "Krawedzie incydentne do wierzcholka " << vertexList[i]->key << ": ";
	for (int j = 0; j < vertexList[i]->adjList2.size(); j++) // petla przechodzaca po krawedziach
	cout << vertexList[i]->adjList2.at(j)->weight << " "; // wyswietlanie kolejnych krawedzi
	cout << endl;
	}
	}  */
	if (edgeList.size() == 0) { // sprawdzenie czy graf nie jest pusty
		cout << "Graf nie ma zadnych krawedzi!\n";
	}
	else {
		cout << endl << "Wszystkie krawedzie w grafie:\n";
		for (int i = 0; i < edgeList.size(); i++)  // petla przechodzaca po wierzcholkach
			cout << edgeList[i]->weight << " ";
		cout << endl;
	}
}

void Graph::incidentEdges(int indeksV)
{
	if (indeksV >= 0 && indeksV < vertexList.size()) { // sprawdzenie czy podany zostal poprawny indeks
		if (vertexList[indeksV]->adjList2.size() == 0) // sprawdzenie czy wierzcholek ma jakiekolwiek krawedzie
			cout << "Ten wierzcholek nie ma zadnych krawedzi incydentnych" << endl;
		else
			cout << "Krawedzie incydentne do wierzcholka " << vertexList[indeksV]->key << ": ";
		for (int i = 0; i < vertexList[indeksV]->adjList2.size(); i++) // petla przechodzaca po krawedziach
			cout << vertexList[indeksV]->adjList2.at(i)->weight << " "; // wyswietlanie kolejnych krawedzi
		cout << endl;
	}
	else
		cout << "Podany wierzcholek nie istnieje!\n";
}

void Graph::endVertices(int indeksE)
{
	if (indeksE >= 0 && indeksE < edgeList.size()) { // sprawdzenie czy podany zostal poprawny indeks
		cout << "Krawedz o wadze " << edgeList[indeksE]->weight << " laczy wierzcholki " << edgeList[indeksE]->first->key << " i " << edgeList[indeksE]->last->key << endl;
	}
	else
		cout << "Podana krawedz nie istnieje!\n";
}

void Graph::opposite(int indeksV, int indeksE)
{
	if (indeksV < 0 || indeksV >= vertexList.size())  // sprawdzenie czy podany zostal poprawny indeks wierzcholka
		cout << "Podany wierzcholek nie istnieje!\n";
	else if (indeksE >= 0 && indeksE < vertexList[indeksV]->adjList2.size()) { // sprawdzenie czy podany zostal poprawny indeks krawedzi
		if (vertexList[indeksV] == vertexList[indeksV]->adjList2[indeksE]->first) // sprawdzenie ktory koniec krawedzi zostal podany
			cout << "Wierzcholek przeciwny do podanego to " << vertexList[indeksV]->adjList2[indeksE]->last->key << endl;
		else
			cout << "Wierzcholek przeciwny do podanego to " << vertexList[indeksV]->adjList2[indeksE]->first->key << endl;
	}
	else
		cout << "Podany wierzcholek nie posiada takiej krawedzi!\n";
}

Vertex* Graph::opposite(Vertex* V, Edge* E)
{
	if (E->first->key == V->key) // sprawdzenie ktory wierzcholek koncowy krawedzi zwrocic
		return E->last;
	else
		return E->first;
}

void Graph::replaceV(int indeksV, int nowyKlucz)
{
	if (indeksV >= 0 && indeksV < vertexList.size()) { // sprawdzenie czy podany zostal poprawny indeks
		int staryKlucz = vertexList[indeksV]->key;
		vertexList[indeksV]->key = nowyKlucz; // przypisanie nowej wartosci klucza
		cout << "Nowa wartosc wierzcholka " << staryKlucz << " to " << nowyKlucz << endl;
	}
	else
		cout << "Podany wierzcholek nie istnieje!\n";
}

void Graph::removeE(int indeksE)
{
	if (indeksE >= 0 && indeksE < edgeList.size()) {
		int waga = edgeList[indeksE]->weight;
		for (int i = 0; i < edgeList[indeksE]->first->adjList2.size(); i++) {
			if (edgeList[indeksE]->first->adjList2[i] == edgeList[indeksE]) // znalezienie i usuniecie krawedzi
				edgeList[indeksE]->first->adjList2.erase(edgeList[indeksE]->first->adjList2.begin() + i);
		}
		for (int i = 0; i < edgeList[indeksE]->last->adjList2.size(); i++) {
			if (edgeList[indeksE]->last->adjList2[i] == edgeList[indeksE]) // znalezienie i usuniecie krawedzi
				edgeList[indeksE]->last->adjList2.erase(edgeList[indeksE]->last->adjList2.begin() + i);
		}
		edgeList.erase(edgeList.begin() + indeksE); // usuniecie krawedzi z listy krawedzi
		cout << "Krawedz o wartosci " << waga << " zostala usunieta\n";// sprawdzenie czy podany zostal poprawny indeks
	}
	else {
		cout << "Podana krawedz nie istnieje!\n";
	}
}

void Graph::removeV(int indeksV)
{
	if (indeksV >= 0 && indeksV < vertexList.size()) { // sprawdzenie czy podany zostal poprawny indeks
		int klucz = vertexList[indeksV]->key;
		while (vertexList[indeksV]->adjList2.size() != 0) { // dopoki wierzcholek ma jakiekolwiek krawedzie incydentne
			for (int i = 0; i < edgeList.size(); i++) {
				if (edgeList[i] == vertexList[indeksV]->adjList2[0]) {
					removeE(i); // usuniecie wszystkich krawedzi incydentnych danego wierzcholka
					break;
				}
			}
			//removeE(0); // usuwanie wszystkich krawedzi incydentnych danego wierzcholka
		}
		vertexList.erase(vertexList.begin() + indeksV); // usuniecie danego wierzcholka z listy wierzcholkow
		cout << "Wierzcholek o wartosci " << klucz << " zostal usuniety\n";
	}
	else
		cout << "Podany wierzcholek nie istnieje!\n";
}