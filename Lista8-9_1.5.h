#pragma once
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Vertex;

class Edge {
public:
	Edge(int waga, Vertex *one, Vertex *two) // konstruktor
	{
		weight = waga; // nadanie wagi
		first = one; // nadanie wierzcholkow ktore laczy dana krawedz
		last = two;
	}
	int weight; // waga krawedzi
	Vertex *first; // jeden koniec krawedzi
	Vertex *last; // drugi koniec krawedzi
	string etykieta; // uzywane przy sprawdzaniu czy graf jest spojny
};

class Vertex {
public:
	Vertex(int klucz) // konstruktor
	{
		key = klucz;
	}
	int key; // wartosc wierzcholka
	int indeks; // indeks macierzy sasiedztwa powiazany z wierzcholkiem
	string etykieta; // uzywane przy sprawdzaniu czy graf jest spojny
	int distance; // odleglosc od wierzcholka poczatkowego (do Dijkstry)
	int index; // indeks w kolejce priorytetowej (do Dijkstry)
	int odleglosc; // aktualna waga krawedzi (do Dijkstry)
	Vertex *last; // wierzcholek od ktorego prowadzi krawedz do tego wierzcholka (do Dijkstry)
};