#include <iostream>
#include <cstdlib>
#include <Windows.h>
#include <ctime>
#define MAX 2147483647 // maksymalny integer
#include "ev.h"
#include "g.h"
#include "mst.h"
#include "h.h"
#include "h2.h"

using namespace std;

void DFS(Graph *graf, Vertex *vertex)
{
	vertex->etykieta = "ODWIEDZONY";
	for (int i = 0; i < vertex->adjList2.size(); i++) { // sprawdzenie kazdej krawedzi incydentnej wierzcholka
		Vertex *V = graf->opposite(vertex, vertex->adjList2[i]);
		if (V->etykieta == "NIEODWIEDZONY") { // sprawdzenie czy wierzcholek zostal juz odwiedzony
			vertex->adjList2[i]->etykieta = "ZNALEZIONY";
			DFS(graf, V); // wywolanie przeszukiwania dla wlasnie odwiedzonego wierzcholka
		}
		else
			vertex->adjList2[i]->etykieta = "POWROTNY";
	}
}

bool DFS(Graph *graf)
{
	int i;
	// ustawienie etykiet wszystkich wierzcholkow i krawedzi jako NIEODWIEDZONY
	for (i = 0; i < graf->vertexList.size(); i++)
		graf->vertexList[i]->etykieta = "NIEODWIEDZONY";
	for (i = 0; i < graf->edgeList.size(); i++)
		graf->edgeList[i]->etykieta = "NIEODWIEDZONY";
	DFS(graf, graf->vertexList[0]); // wywolanie przeszukiwania w glab dla jednego z wierzcholkow
	for (i = 0; i < graf->vertexList.size(); i++) {
		if (graf->vertexList[i]->etykieta != "ODWIEDZONY") {// jesli ktorys z wierzcholkow nie zostal odwiedzony to graf nie jest spojny
			cout << "Graf nie jest spojny!\n";
			return false; // zwraca false gdy graf nie jest spojny
		}
	}
	return true; // zwraca true gdy graf jest spojny
}

void randomize(Graph *graf, Graph *MST, int vertices, float density)
{
	while (1) {
		for (int i = 0; i < vertices; i++) {
			graf->addVertex(i); // dodanie wierzcholka
		}
		int edges = (density / 100.0f * vertices * (vertices - 1)) / 2; // obliczenie ilosci krawedzi w grafie
		for (int i = 0; i < edges; i++) {
			int waga, indeks1, indeks2;
			waga = rand() % 1000; // losowanie wagi krawedzi
			indeks1 = rand() % vertices; // losowanie indeksow wierzcholkow koncowych krawedzi
			indeks2 = rand() % vertices;
			graf->addEdge2(waga, indeks1, indeks2); // dodanie krawedzi
		}
		if (DFS(graf) == true) // sprawdzenie czy graf jest spojny
			break;
		else
			cout << "Jedziemy dalej\n";
	}
}

void kruskal(Graph *graf, Graph *MST, Heap *queue, int vertices)
{
	klaster claster(vertices);
	for (int i = 0; i < graf->vertexList.size(); i++) {
		claster.makeK(graf->vertexList[i]); // wszystkie wezly stanowia osobne klastry
		MST->addVertex(i); // dodanie wierzcholka
	}
	for (int i = 0; i < graf->edgeList.size(); i++) {
		queue->insert(graf->edgeList[i]); // wlozenie wszystkich krawedzi do kolejki priorytetowej
	}
	//queue->display();
	Edge *edge;
	for (int i = 1; i < graf->vertexList.size(); i++) { // n-1 powtorzen, czyli minimalna liczba krawedzi w grafie by byl on spojny
		do {
			edge = queue->front(); // pierwszy element kolejki
			queue->removeMin(); // usuniecie pierwszego elementu
		} while (claster.findK(edge->first) == claster.findK(edge->last)); // dopoki dwie krawedzi zewnetrzne naleza do tego samego klastra
		MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
																		//claster.display(vertices);
		claster.unionK(edge); // polaczenie klastrow zawierajacych dwie krawedzie zewnetrzne
	}
	//MST->vertices();
	//MST->edges();
}

void prim(Graph *graf, Graph *MST, Heap *queue, int vertices)
{
	int verticesCount = 1; // do sprawdzania czy juz dodano wszystkie wierzcholki
	bool *mstSet = new bool[vertices]; // do sprawdzania czy dany wierzcholek jest juz w drzewie MST
	for (int i = 0; i < graf->vertexList.size(); i++) {
		mstSet[i] = false; // ustawienie wszystkich komorek tablicy mstSet na false
	}
	mstSet[0] = true;
	MST->addVertex(0); // wierzcholek poczatkowy
	for (int i = 0; i < graf->vertexList[0]->adjList2.size(); i++)
		queue->insert(graf->vertexList[0]->adjList2[i]); // dodanie krawedzi incydentnych wierzcholka poczatkowego do kolejki priorytetowej
	Edge *edge;
	Vertex *vertex;
	while (verticesCount < vertices) { // dopoki nie wszystkie krawedzie sa w drzewie MST
									   //queue->display();
									   //cout << endl;
		edge = queue->front(); // pierwszy element kolejki
		queue->removeMin(); // usuniecie pierwszego elementu
		if (mstSet[edge->first->key] == false || mstSet[edge->last->key] == false) { // sprawdzenie czy wierzcholki tej krawedzi sa juz w drzewie MST
			if (mstSet[edge->first->key] == false) { // sprawdzenie ktorej krawedzi nie ma w drzewie
				vertex = graf->vertexList[edge->first->key];
				mstSet[edge->first->key] = true;
				MST->addVertex(edge->first->key); // dodanie wierzcholka do drzewa MST
				verticesCount++; // zwiekszenie liczby wierzcholkow o jeden
				MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
				for (int i = 0; i < vertex->adjList2.size(); i++) {
					if (mstSet[vertex->adjList2[i]->first->key] == false || mstSet[vertex->adjList2[i]->last->key] == false)
						queue->insert(vertex->adjList2[i]); // dodanie krawedzi do kolejki priorytetowej jesli jeszcze sie na niej nie znajduje
				}
			}
			else {
				vertex = graf->vertexList[edge->last->key];
				mstSet[edge->last->key] = true;
				MST->addVertex(edge->last->key); // dodanie wierzcholka do drzewa MST
				verticesCount++; // zwiekszenie liczby wierzcholkow o jeden
				MST->addEdge2(edge->weight, edge->first->key, edge->last->key); // dodanie krawedzi do drzewa MST
				for (int i = 0; i < vertex->adjList2.size(); i++) {
					if (mstSet[vertex->adjList2[i]->first->key] == false || mstSet[vertex->adjList2[i]->last->key] == false)
						queue->insert(vertex->adjList2[i]); // dodanie krawedzi do kolejki priorytetowej jesli jeszcze sie na niej nie znajduje
				}
			}
		}
	}
	//MST->vertices();
	//MST->edges();
}

void dijkstra(Graph *graf, Graph *MST, Heap2 *queue, int vertices)
{
	graf->vertexList[0]->distance = 0; // ustawienie dystansu dla poczatkowego wierzcholka na 0
	queue->insert(graf->vertexList[0]); // wlozenie wierzcholka do kolejki priorytetowej
	MST->addVertex(0); // dodanie poczatkowego wierzcholka do drzewa najkrotszych sciezek
	Vertex *vertex, *temp;
	int odleglosc;
	int a = MAX; 
	for (int i = 1; i < graf->vertexList.size(); i++) {
		graf->vertexList[i]->distance = a; // ustawienie dystansu pozostalych wierzcholkow na nieskonczonosc
		queue->insert(graf->vertexList[i]); // wlozenie pozostalych wierzcholkow do kolejki priorytetowej
		//cout << "Insertuje: " << graf->vertexList[i]->key << endl;
		//queue->display();
	}
	while (queue->rozmiar > 0) {
		vertex = queue->front();
		queue->removeMin(); // usuniecie pierwszego elementu z kolejki
		//cout << "Dodaje: " << vertex->key << endl;
		//queue->display();
		MST->addVertex(vertex->key); // dodanie wierzcholka do drzewa najkrotszych sciezek
		for (int i = 0; i < vertex->adjList2.size(); i++) { // wszystkie krawedzie incydentne danego wierzcholka
			if (vertex->adjList2[i]->first == vertex) // znalezienie wierzcholka przeciwleglego
				temp = vertex->adjList2[i]->last;
			else
				temp = vertex->adjList2[i]->first;
			odleglosc = vertex->distance + vertex->adjList2[i]->weight; // dodanie do jego obecnego dystansu wagi krawedzi laczacej wierzcholki
			if (odleglosc < temp->distance) { // sprawdzenie czy nowa sciezka jest krotsza od obecnej najkrotszej
				queue->changeKey(temp, odleglosc);  // zmiana klucza
				temp->last = vertex; // ustawienie wierzcholka przeciwleglego
				temp->odleglosc = vertex->adjList2[i]->weight;
			}
		}
	}
	cout << endl;
	for (int i = 1; i < graf->vertexList.size(); i++) {
		cout << "Dodaje krawedz " << graf->vertexList[i]->odleglosc << " do wierzcholkow " << i << " i " << graf->vertexList[i]->last->key << endl;
		MST->addEdge2(graf->vertexList[i]->odleglosc, i, graf->vertexList[i]->last->key);
	}  
	cout << endl << "Odleglosci od wierzcholka poczatkowego:\n";
	for (int i = 0; i < graf->vertexList.size(); i++) {
	   cout << graf->vertexList[i]->distance << " ";
	} 
	cout << endl;
}

int main()
{
	cout << "LISTA SASIEDZTWA\n\n";
	int vertices, density; // liczba wierzcholkow i gestosc tworzonego grafu
	//cout << "Ile wierzcholkow ma byc w grafie? ";
	//cin >> vertices; 
	vertices = 10;
	//cout << "Jak gesty procentowo ma byc graf? ";
	//cin >> density;
	density = 100;

	Graph *graf = new Graph(vertices);
	Graph *graf2 = new Graph(vertices);
	Graph *graf3 = new Graph(vertices);

	Graph *MST = new Graph(vertices); // minimalne drzewo rozpinajace
	Graph *MST2 = new Graph(vertices);
	Graph *MST3 = new Graph(vertices);

	int edges = (density / 100.0f * vertices * (vertices - 1)) / 2;

	Heap *heap = new Heap(edges); // kolejka priorytetowa dla krawedzi
	Heap *heap2 = new Heap(edges); // kolejka priorytetowa dla krawedzi
	Heap2 *heap3 = new Heap2(vertices); // kolejka priorytetowa dla wierzcholkow
	/*
	//srand(time(NULL));
	randomize(graf, MST, vertices, density);
	randomize(graf2, MST2, vertices, density);
	
	LARGE_INTEGER frequency; // czestotliwosc
	LARGE_INTEGER t1, t2; // czas
	double Time; // wyliczony czas dzialania algorytmu
	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	//kruskal(graf, MST, heap, vertices);
	dijkstra(graf, MST, heap3, vertices);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
	cout << "Czas dla algorytmu Kruskala: " << Time << " ms.\n";

	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	dijkstra(graf2, MST2, heap3, vertices);
	//prim(graf2, MST2, heap2, vertices);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
	cout << "Czas dla algorytmu Prima: " << Time << " ms.\n";
	*/
	
	graf->addVertex(0);
	graf->addVertex(1);
	graf->addVertex(2);
	graf->addVertex(3);
	graf->addVertex(4);
	graf->addVertex(5);
	graf->addVertex(6);
	graf->addVertex(7);
	graf->addVertex(8);
	graf->addVertex(9); 
	graf->addEdge2(11, 0, 1);
	graf->addEdge2(15, 1, 2);
	graf->addEdge2(12, 3, 5);
	graf->addEdge2(14, 8, 7);
	graf->addEdge2(18, 6, 3);
	graf->addEdge2(26, 7, 4);
	graf->addEdge2(23, 9, 8);
	graf->addEdge2(28, 0, 6);
	graf->addEdge2(21, 6, 1);
	graf->addEdge2(27, 3, 2);
	graf->addEdge2(30, 9, 5);
	graf->addEdge2(5, 2, 4);
	graf->addEdge2(29, 0, 7);
	graf->addEdge2(37, 5, 2);
	graf->addEdge2(38, 3, 1);
	cout << "Graf poczatkowy:"; 
	graf->vertices();
	graf->edges();
	dijkstra(graf, MST, heap3, vertices); 
	//graf2 = graf;
	//cout << "\nMST dla algorytmu Prima:\n";
	//prim(graf, MST, heap, vertices);
	//cout << "\nMST dla algorytmu Kruskala:\n";
	//kruskal(graf2, MST2, heap2, vertices);  
}